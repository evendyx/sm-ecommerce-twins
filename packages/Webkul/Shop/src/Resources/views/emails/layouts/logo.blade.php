@if (core()->getConfigData('general.design.admin_logo.logo_image'))
    <img src="{{ \Illuminate\Support\Facades\Storage::url(core()->getConfigData('general.design.admin_logo.logo_image')) }}" title="TwinCollection" alt="TwinsCollection Logo" style="display:block; height: 40px; width: 110px;"/>
@else
    <img src="{{ bagisto_asset('images/logo.png') }}" title="TwinCollection" alt="TwinsCollection Logo" style="display:block;">
@endif