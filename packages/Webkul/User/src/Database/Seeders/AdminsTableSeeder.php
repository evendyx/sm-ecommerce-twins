<?php

namespace Webkul\User\Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class AdminsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('admins')->delete();

        DB::table('admins')->insert([
            [
                'id' => 1,
                'name' => 'Admin',
                'email' => 'admin@store.mulaitrip.com',
                'password' => bcrypt('qazxcvbnm@admin'),
                'status' => 1,
                'role_id' => 1,
            ],
            [
                'id' => 2,
                'name' => 'Admin 2',
                'email' => 'services@store.mulaitrip.com',
                'password' => bcrypt('qazxcvbnm@services'),
                'status' => 1,
                'role_id' => 2,
            ]
        ]);
    }
}
